package com.taptapnetworks.codetest;

public class SnippetToRefactor {
    public void send(String type, String message, String to, String subject) {
        if (type == "SMS") {
            SMPP smpp = new SMPP("192.168.1.4", "4301");
            smpp.openConnection("your-username", "your-password");
            smpp.send(to, message);
            smpp.closeConnection();
        } else if (type == "mail") {
            SendMailConnection smc = new SendMailConnection("mail.myserver.com");
            smc.prepareMessage(to, message, subject);
            smc.send();
            smc.close();
        } else {
            throw new RuntimeException("Type is unknown");
        }
    }
}
